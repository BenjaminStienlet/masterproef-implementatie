
local validator = require("validate_misc")

local cudf = "rand1bff33"
local input = "cudf_input/misc/misc2010/results/problems/easy/" .. cudf ..".cudf"
-- local file1 = "cudf_input/misc/misc2010/results/solutions/uns-paranoid-0.0002/" .. cudf .. ".cudf.easy.result"
-- local file2 = "cudf_output/easy/" .. cudf .. ".cudf.1.result"
local file1 = "tmp_output/rand1bff33.cudf.1.result"
local file2 = "cudf_output/dep_res_v2/easy/rand1bff33.cudf.1.result"

return validator.validate(input, file1, file2)