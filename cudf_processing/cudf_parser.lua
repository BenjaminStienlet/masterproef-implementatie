-- This file contains a parser for CUDF files
-- The parser returns a lua table which contains the interpreted CUDF data

local parser = { }
local debug = 0

-- Parses the given CUDF file
function parser.parse_cudf( filename )
    if debug > 0 then io.stderr:write(">>> Parser: reading lines") end
    local lines = read_lines(filename)
    if debug > 0 then io.stderr:write(">>> Parser: creating stanzas") end
    local stanzas = create_stanzas(lines)
    if debug > 0 then io.stderr:write(">>> Parser: parsing stanzas") end
    return parse_stanzas(stanzas)
end

-- Return an array with the lines in the file with the given filename
function read_lines( filename )
    local file = assert(io.open(filename, "r"))
    local lines = { }
    while true do
        local line = file:read("*line")
        if line ~= nil then
            table.insert(lines, line)
        else
            break
        end
    end

    return lines
end

-- Return an array with the different stanzas which each start with a postmark
-- The stanzas are created by combining consecutive lines in the given array
function create_stanzas( lines ) 
    local stanzas = { }
    local current_lines = { }
    for i, line in pairs(lines) do
        if not string.match(line, "^%s*$") then -- Not an empty line
            local match = false
            for j, postmark in pairs(postmarks) do
                if line_matches_postmark(line, postmark.postmark) then
                    -- Save the current stanza and start a new one
                    if not table_empty(current_lines) then
                        table.insert(stanzas, current_lines)
                    end
                    current_lines = { line }
                    match = true
                end
            end
            if not match then
                table.insert(current_lines, line)
            end
        end
    end
    if not table_empty(current_lines) then
        table.insert(stanzas, current_lines)
    end
    return stanzas
end

-- Check if the given table is empty
function table_empty( myTable )
    return next(myTable) == nil
end

-- Check if the given line starts with the given postmark
function line_matches_postmark( line, postmark )
    if string.match(line, "^" .. postmark .. ":%s.*$") == nil then
        return false
    else
        return true
    end
end

-- Match the first line of each stanza to one of the postmarks and parse it
function parse_stanzas( stanzas )
    result = { }
    for i, postmark in pairs(postmarks) do
        result[postmark.postmark] = { }
    end
    for i, stanza in pairs(stanzas) do
        for j, postmark in pairs(postmarks) do
            if line_matches_postmark(stanza[1], postmark.postmark) then
                
                if debug > 1 then
                    io.stderr:write(">>> Parsing stanza: ")
                    for i, v in ipairs(stanza) do io.stderr:write(i, v) end
                end
                parsed = postmark.func(stanza)
                if parsed ~= nil then
                    table.insert(result[postmark.postmark], parsed)
                end
            end
        end
    end
    return result
end

-- Parse the given preamble stanza
function parse_preamble( stanza )
    -- Possible keys: property, univ-checksum, ...
    -- TODO: Currently it isn't needed to parse these keys
    return nil
end

-- Parse the given package stanza
function parse_package( stanza )
    local dep, dep_or, pvc = parse_package_formula("true!")
    local package = {
        installed = false,
        depends = dep, 
        depends_or = { },
        constraints = { },
        conflicts = { },
        provides = { },
        keep = parse_keep_enum("none")
    }

    for i, line in pairs(stanza) do
        local key, value = string.match(line, "^(%a+):%s*(.*)$")
        key = string_trim(key)
        value = string_trim(value)
        local pvc = { }

        if key == "package" then 
            if string.match(string_trim(value), "^" .. package_char_class .. "+$") then
                package.package = string_trim(value)
            end
        elseif key == "version" then
            package.version = tonumber(value)
        elseif key == "installed" then
            package.installed = toboolean(value)
        elseif key == "depends" then
            package.depends, package.depends_or, pvc = parse_package_formula(value)
        elseif key == "conflicts" then
            package.conflicts = parse_package_list(value)
            pvc = package.conflicts
        elseif key == "provides" then 
            package.provides = parse_provides_list(value)
            pvc = package.provides
        elseif key == "keep" then
            package.keep = parse_keep_enum(value)
        end
        for j, v in pairs(pvc) do
            table.insert(package.constraints, v)
        end
    end

    if package.package == nil or package.version == nil then 
        io.stderr:write(">>> Error (cudf_parser.lua): parse_package\n")
    end

    return package
end

-- Convert the given string representation of a boolean to a boolean value
function toboolean( bool_string )
    if string.match(bool_string, "^true%s*$") then
        return true
    elseif string.match(bool_string, "^false%s*$") then
        return false
    else
        io.stderr:write(">>> Error (cudf_parser.lua): toboolean (" .. bool_string .. ")\n")
        return false
    end
end

trueDep = { "TrueDep()" }
falseDep = { "FalseDep()" }
-- Convert the given formula to an element of type Dependency
function parse_package_formula( formula )
    formula = string_trim(formula)
    if string.match(formula, "^true!$") then
        return trueDep, { }, { }
    elseif string.match(formula, "^false!$") then
        return falseDep, { }, { }
    else
        local depends = { }
        local depends_or = { }
        local constraints = { }
        for or_formula in string.gmatch(formula, "([^,]+)") do
            local dep, dep_or, pvc = parse_or_dependency(or_formula)
            table.insert(depends, dep)
            for i, v in pairs(dep_or) do
                table.insert(depends_or, v)
            end
            for i, v in pairs(pvc) do
                table.insert(constraints, v)
            end
        end
        return depends, depends_or, constraints
    end
end

emptyOrDep = { constraint = "EmptyOrDep()" }
-- Convert the given OR-formula to an element of type OrDependency
function parse_or_dependency( formula )
    formula = string_trim(formula)
    if string.match(formula, "^([^|]+)$") then
        local constraint = string.match(formula, "^([^|]+)$")
        local parsed_constraint = parse_package_constraint(constraint)
        local dependency = { constraint = parsed_constraint, next_dep = emptyOrDep }
        local rest_depends_or = { emptyOrDep }
        table.insert(rest_depends_or, dependency)
        return dependency, rest_depends_or, { parsed_constraint }
    elseif string.match(formula, "^([^|]+)|(.*)$") then
        local constraint, rest = string.match(formula, "^([^|]+)|(.*)$")
        local rest_dependency, rest_depends_or, constraints = parse_or_dependency(rest)
        local parsed_constraint = parse_package_constraint(constraint)
        local dependency = { constraint = parsed_constraint, next_dep = rest_dependency }
        table.insert(rest_depends_or, dependency)
        table.insert(constraints, parsed_constraint)
        return dependency, rest_depends_or, constraints
    else
        io.stderr:write(">>> Error (cudf_parser.lua): parse_or_dependency (" .. formula .. ")\n")
    end
end

-- Convert the string to a list of elements of type PackageVersionConstraint
function parse_package_list( list )
    local result = { }
    for constraint in string.gmatch(list, "([^,]+)") do
        local parsed_constraint = parse_package_constraint(constraint)
        table.insert(result, parsed_constraint)
    end
    return result
end

-- Convert the string to an element of type PackageVersionConstraint 
function parse_package_constraint( constraint )
    constraint = string_trim(constraint)

    local name, version = string.match(constraint, "^(" .. package_char_class .. "+)%s*=%s*(%d+)$")
    if name and version then
        return { name = name, rel = "EQ()", version = tonumber(version) }
    end
    local name, version = string.match(constraint, "^(" .. package_char_class .. "+)%s*!=%s*(%d+)$")
    if name and version then
        return { name = name, rel = "NEQ()", version = tonumber(version) }
    end
    local name, version = string.match(constraint, "^(" .. package_char_class .. "+)%s*>=%s*(%d+)$")
    if name and version then
        return { name = name, rel = "GE()", version = tonumber(version) }
    end
    local name, version = string.match(constraint, "^(" .. package_char_class .. "+)%s*>%s*(%d+)$")
    if name and version then
        return { name = name, rel = "GT()", version = tonumber(version) }
    end
    local name, version = string.match(constraint, "^(" .. package_char_class .. "+)%s*<=%s*(%d+)$")
    if name and version then
        return { name = name, rel = "LE()", version = tonumber(version) }
    end
    local name, version = string.match(constraint, "^(" .. package_char_class .. "+)%s*<%s*(%d+)$")
    if name and version then
        return { name = name, rel = "LT()", version = tonumber(version) }
    end
    local name = string.match(constraint, "^(" .. package_char_class .. "+)$")
    if name then
        return { name = name }
    end
    io.stderr:write(">>> Error (cudf_parser.lua): parse_package_constraint (" .. constraint .. ")\n")
end

-- Convert the string to a list of elements of type VirtualPackage
function parse_provides_list( list )
    local result = { }
    for constraint in string.gmatch(list, "([^,]+)") do
        local parsed_constraint = parse_versioned_package(constraint)
        table.insert(result, parsed_constraint)
    end
    return result
end

-- Convert the string to an element of type VirtualPackage 
function parse_versioned_package( constraint )
    constraint = string_trim(constraint)

    local name, version = string.match(constraint, "^(" .. package_char_class .. "+)%s+=%s+(%d+)$")
    if name and version then
        return { name = name, version = tonumber(version) }
    end
    local name = string.match(constraint, "^(" .. package_char_class .. "+)$")
    if name then
        return { name = name }
    end
    io.stderr:write(">>> Error (cudf_parser.lua): parse_versioned_package (" .. constraint .. ")\n")
end

-- Convert the string to an element of the KeepType
function parse_keep_enum( value )
    if string.match(value, "^version%s*$") then
        return "Version()"
    elseif string.match(value, "^package%s*$") then
        return "Package()"
    elseif string.match(value, "^feature%s*$") then
        return "Feature()"
    elseif string.match(value, "^none%s*$") then
        return "None()"
    else 
        io.stderr:write(">>> Error (cudf_parser.lua): parse_keep_enum (" .. value .. ")\n")
    end
end

-- Parse the given request stanza
function parse_request( stanza )
    local request = {
        install = { },
        remove = { },
        upgrade = { },
        constraints = { }
    }

    for i, line in pairs(stanza) do
        local key, value = string.match(line, "^(%a+):%s*(.*)$")
        key = string_trim(key)
        value = string_trim(value)
        local pvc = { }
        if key == "request" then 
            -- Do nothing; value can be nothing or a url
        elseif key == "install" then 
            request.install = parse_package_list(value)
            pvc = request.install
        elseif key == "remove" then 
            request.remove = parse_package_list(value)
            pvc = request.remove
        elseif key == "upgrade" then 
            request.upgrade = parse_package_list(value)
            pvc = request.upgrade
        end
        for j, v in pairs(pvc) do
            table.insert(request.constraints, v)
        end
    end

    return request
end

-- Trim spaces at the start and end of the given string
function string_trim( s )
    if (s == nil) then
        return s
    end
    local i = 1
    local j = s:len()
    while s:sub(i,i) == " " do
        i = i + 1
    end
    while s:sub(j,j) == " " do
        j = j - 1
    end

    return s:sub(i,j)
end

-- The postmarks that occur at the beginning of stanzas
postmarks = {
    {postmark="preamble", func=parse_preamble}, 
    {postmark="package", func=parse_package}, 
    {postmark="request", func=parse_request}
}

-- Package name: a-z, A-Z, -, +, ., /, %
package_char_class = "[a-zA-Z0-9%-+%./%%]"

-- temporary for testing
-- parser.parse_cudf("cudf_input/legacy.cudf")
-- dependency, depends_or = parse_package_formula("glass != 1, engine >= 3 | door = 1")
-- for i, dep in pairs(dependency) do
--     print("Dependency: " .. dep)
-- end
-- for i, dep in pairs(depends_or) do
--     print("DependsOr: " .. dep)
-- end

return parser