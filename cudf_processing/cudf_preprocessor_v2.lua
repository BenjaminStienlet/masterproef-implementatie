-- The preprocessor reduces the size of the problem by removing packages that
-- certainly won't be needed to fulfil the request
-- The preprocessor uses the output from the CUDF parser

local cudf_preprocessor = { }
local debug = 0

-- Preprocess the given CUDF interpretation that has been parsed by cudf_parser.lua
-- This outputs a subset of the given interpretation with only the packages that are
-- needed to solve the given request
function cudf_preprocessor.process( cudf_interpretation )
    if debug > 0 then io.stderr:write(">>> Preprocessor: get provide list\n") end
    local provide_list = cudf_preprocessor.get_provide_list(cudf_interpretation)
    if debug > 0 then io.stderr:write(">>> Preprocessor: get package cache\n") end
    local package_cache = cudf_preprocessor.get_package_cache(cudf_interpretation)
    if debug > 0 then io.stderr:write(">>> Preprocessor: get dependency graph\n") end
    local graph = cudf_preprocessor.get_dependency_graph(cudf_interpretation, package_cache, provide_list)
    if debug > 0 then io.stderr:write(">>> Preprocessor: get begin set\n") end
    local begin_set = get_begin_set(cudf_interpretation, package_cache, provide_list)
    if debug > 0 then io.stderr:write(">>> Preprocessor: get transitive closure\n") end
    local result = get_transitive_closure(cudf_interpretation, graph, begin_set, package_cache)
    
    cudf_interpretation["package"] = result
    return cudf_interpretation
end

-- Get a list of the provided packages
-- Mapping from the virtual package name to a list of 
-- { pvc of the provided package, package that provides it }
function cudf_preprocessor.get_provide_list( cudf_interpretation )
    local provides = { }
    for k1, pkg in pairs(cudf_interpretation["package"]) do
        for k2, prov in pairs(pkg["provides"]) do
            local pkg_representation = { pvc = prov, package = pkg }
            if provides[prov["name"]] == nil then
                provides[prov["name"]] = { pkg_representation }
            else
                table.insert(provides[prov["name"]], pkg_representation)
            end
        end
    end
    return provides
end

-- Get the package cache that is used by other methods to reduce the time needed
-- to find packages that satisfy a package version constraint
-- Format: table[package name][package version] -> package
-- This allows fast lookup of all packages with a certain name or to get all information 
-- of a specific package by giving a package name and version
function cudf_preprocessor.get_package_cache( cudf_interpretation )
    local cache = { }
    for k, pkg in pairs(cudf_interpretation["package"]) do
        if cache[pkg["package"]] == nil then
            cache[pkg["package"]] = {}
        end
        cache[pkg["package"]][pkg["version"]] = pkg
    end
    return cache
end

-- Get the dependency graph that is used to find the transitive closure for a given set
-- Mapping from a versioned package to all packages with which it has a relation 
-- (depends, conflicts or a relation via a virtual package)
function cudf_preprocessor.get_dependency_graph( cudf_interpretation, package_cache, provide_list )
    local graph = { }
    for k1, pkg in pairs(cudf_interpretation["package"]) do
        local relations = { }
        local versioned_package = get_versioned_package(pkg["package"], pkg["version"])
        
        for k2, dep in pairs(pkg["depends"]) do
            if dep ~= "TrueDep()" and dep ~= "FalseDep()" then
                while dep["constraint"] ~= "EmptyOrDep()" do
                    relations = insert_all(relations, cudf_preprocessor.get_valid_packages(dep["constraint"], package_cache, provide_list))
                    dep = dep["next_dep"]
                end
            end
        end

        for k2, conf in pairs(pkg["conflicts"]) do
            relations = insert_all(relations, cudf_preprocessor.get_valid_packages(conf, package_cache, provide_list))
        end

        graph[versioned_package] = relations
    end
    return graph
end

-- Get all packages from the package cache and provide list that satisfy the given
-- package version constraint
function cudf_preprocessor.get_valid_packages( pvc, package_cache, provide_list )
    local packages = { }
    if package_cache[pvc["name"]] ~= nil then
        for k, pkg in pairs(package_cache[pvc["name"]]) do
            if satisfies_constraint(pkg, pvc) then
                table.insert(packages, pkg)
            end
        end
    end
    if provide_list[pvc["name"]] ~= nil then
        for k, pkg_rep in pairs(provide_list[pvc["name"]]) do
            if satisfies_provide(pkg_rep["pvc"], pvc) then
                table.insert(packages, pkg_rep["package"])
            end
        end
    end
    return packages
end

-- Check whether the given package satisfies the package version constraint
function satisfies_constraint( pkg, pvc )
    if pvc["rel"] == nil or pvc["version"] == nil then
        return pvc["name"] == pkg["package"]
    else
        return pvc["name"] == pkg["package"] and relation_holds(pkg["version"], pvc["rel"], pvc["version"])
    end
end

-- Check whether the given provided virtual package (given as a pvc) satisfies the
-- package version constraint
function satisfies_provide( prov, pvc ) 
    if prov["rel"] == nil or prov["version"] == nil or pvc["rel"] == nil or pvc["version"] == nil then
        return prov["name"] == pvc["name"]
    else
        return pvc["name"] == prov["name"] and relation_holds(prov["version"], pvc["rel"], pvc["version"]) 
            and relation_holds(pvc["version"], prov["rel"], prov["version"])
    end
end

-- Check whether the given relation holds between the two version numbers
function relation_holds( v1, rel, v2 )
    if rel == "EQ()" then
        return v1 == v2
    elseif rel == "NEQ()" then
        return v1 ~= v2
    elseif rel == "GE()" then
        return v1 >= v2
    elseif rel == "GT()" then
        return v1 > v2
    elseif rel == "LE()" then
        return v1 <= v2
    elseif rel == "LT()" then
        return v1 < v2
    else 
        io.stderr:write(">>> Error (cudf_preprocessor.lua): relation_holds (" .. rel .. ")\n")
    end
end

-- Get the set that is used to start finding the transitive closure
function get_begin_set( cudf_interpretation, package_cache, provide_list )
    local begin_set = { }
    begin_set = insert_all(begin_set, get_request_set(cudf_interpretation, package_cache, provide_list, "install"))
    begin_set = insert_all(begin_set, get_request_set(cudf_interpretation, package_cache, provide_list, "upgrade"))
    begin_set = insert_all(begin_set, get_request_set(cudf_interpretation, package_cache, provide_list, "remove"))
    -- begin_set = insert_all(begin_set, get_installed_set(cudf_interpretation))
    return begin_set
end

-- Get a list of packages in the cudf interpretation that fulfill on of the requests 
-- for the given request type
function get_request_set( cudf_interpretation, package_cache, provide_list, request_type )
    local result = { }
    local type_set = cudf_interpretation["request"][1][request_type]
    for k, pvc in pairs(type_set) do
        result = insert_all(result, cudf_preprocessor.get_valid_packages(pvc, package_cache, provide_list))  
    end
    return result
end

-- Get the set of packages that are currently installed
function get_installed_set( cudf_interpretation )
    local set = { }
    for k, pkg in pairs(cudf_interpretation["package"]) do
        if pkg["installed"] then
            table.insert(set, pkg)
        end
    end
    return set
end

-- Get the transitive closure of the given set
function get_transitive_closure( cudf_interpretation, graph, set, package_cache )
    local result = { }
    for _, pkg in pairs(set) do
        result[get_versioned_package(pkg["package"], pkg["version"])] = pkg
    end
    local installed = get_installed_set(cudf_interpretation)
    local last_added = set
    while not table_empty(last_added) do
        -- Add packages from installed
        for _, pkg in pairs(installed) do
            local vp = get_versioned_package(pkg["package"], pkg["version"])
            for _, dep in pairs(graph[vp]) do
                -- if dep in last_added
                if last_added[get_versioned_package(dep["package"], dep["version"])] ~= nil then
                    if result[vp] == nil then
                        result[vp] = pkg
                        last_added[vp] = pkg
                    end
                end
            end
        end

        -- Add packages with which the packages in last_added are related
        add = { } 
        for _, pkg in pairs(last_added) do
            for _, dep in pairs(graph[get_versioned_package(pkg["package"], pkg["version"])]) do
                local vp = get_versioned_package(dep["package"], dep["version"])
                if result[vp] == nil then
                    result[vp] = dep
                    add[vp] = dep
                end
            end
        end
        last_added = add
    end
    return result
end

-- Check whether the given table is empty
function table_empty( myTable )
    return next(myTable) == nil
end

-- Get the versioned package representation, used as key in some tables
function get_versioned_package( package, version ) 
    return package .. "-" .. version
end

-- Insert all elements of the given set in the table
function insert_all( tab, set )
    for k, v in pairs(set) do
        table.insert(tab, v)
    end
    return tab
end

return cudf_preprocessor