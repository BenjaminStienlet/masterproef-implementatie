  <!doctype html>
  <html>
    <head>
      <title>README - Lupa</title>
      <link rel="stylesheet" href="style.css" type="text/css" />
      <link rel="icon" href="icon.png" type="image/png" />
      <meta charset="utf-8" />
    </head>
    <body>
      <div id="content">
        <div id="header">
          <h1>Lupa</h1>

<ul>
<li><a href="README.html">Home</a> |</li>
<li><a href="http://foicica.com/lupa/download">Download</a> |</li>
<li><a href="api.html">API</a> |</li>
<li><a href="http://foicica.com/hg/lupa">Source</a> |</li>
<li><a href="http://foicica.com/lists">Mailing List</a></li>
</ul>


        </div>
        <div id="main">
          <h1 id="Lupa">Lupa</h1>

<h2 id="Introduction">Introduction</h2>

<p>Lupa is a <a href="http://jinja.pocoo.org">Jinja2</a> template engine implementation written in Lua and supports
Lua syntax within tags and variables.</p>

<p>Lupa was sponsored by the <a href="http://www.uantwerpen.be/">Library of the University of Antwerp</a>.</p>

<h2 id="Requirements">Requirements</h2>

<p>Lupa has the following requirements:</p>

<ul>
<li><a href="http://www.lua.org">Lua</a> 5.1, 5.2, or 5.3.</li>
<li>The <a href="http://www.inf.puc-rio.br/~roberto/lpeg/">LPeg</a> library.</li>
</ul>


<h2 id="Download">Download</h2>

<p>Download Lupa from the project’s <a href="download">download page</a>.</p>

<h2 id="Installation">Installation</h2>

<p>Unzip Lupa and place the &ldquo;lupa.lua&rdquo; file in your Lua installation&rsquo;s
<code>package.path</code>. This location depends on your version of Lua. Typical locations
are listed below.</p>

<ul>
<li>Lua 5.1: <em>/usr/local/share/lua/5.1/</em> or <em>/usr/local/share/lua/5.1/</em></li>
<li>Lua 5.2: <em>/usr/local/share/lua/5.2/</em> or <em>/usr/local/share/lua/5.2/</em></li>
<li>Lua 5.3: <em>/usr/local/share/lua/5.3/</em> or <em>/usr/local/share/lua/5.3/</em></li>
</ul>


<p>You can also place the &ldquo;lupa.lua&rdquo; file wherever you&rsquo;d like and add it to Lua&rsquo;s
<code>package.path</code> manually in your program. For example, if Lupa was placed in a
<em>/home/user/lua/</em> directory, it can be used as follows:</p>

<pre><code>package.path = package.path..';/home/user/lua/?.lua'
</code></pre>

<h2 id="Usage">Usage</h2>

<p>Lupa is simply a Lua library. Its <code>lupa.expand()</code> and <code>lupa.expand_file()</code>
functions may called to process templates. For example:</p>

<pre><code>lupa = require('lupa')
lupa.expand("hello {{ s }}!", {s = "world"}) --&gt; "hello world!"
lupa.expand("{% for i in {1, 2, 3} %}{{ i }}{% endfor %}") --&gt; 123
</code></pre>

<p>By default, Lupa loads templates relative to the current working directory. This
can be changed by reconfiguring Lupa:</p>

<pre><code>lupa.expand_file('name') --&gt; expands template "./name"
lupa.configure{loader = lupa.loaders.filesystem('path/to/templates')}
lupa.expand_file('name') --&gt; expands template "path/to/templates/name"
</code></pre>

<p>See Lupa&rsquo;s <a href="api.html">API documentation</a> for more information.</p>

<h2 id="Syntax">Syntax</h2>

<p>Please refer to Jinja2&rsquo;s extensive <a href="http://jinja.pocoo.org/docs/dev/templates/">template documentation</a>. Any
incompatibilities are listed in the sections below.</p>

<h2 id="Comparison.with.Jinja2">Comparison with Jinja2</h2>

<p>While Lua and Python (Jinja2&rsquo;s implementation language) share some similarities,
the languages themselves are fundamentally different. Nevertheless, a
significant effort was made to support a vast majority of Jinja2&rsquo;s Python-style
syntax. As a result, Lupa passes Jinja2&rsquo;s test suite with only a handful of
modifications. The comprehensive list of differences between Lupa and Jinja2 is
described in the following sections.</p>

<h3 id="Fundamental.Differences">Fundamental Differences</h3>

<ul>
<li><p>Expressions use Lua&rsquo;s syntax instead of Python&rsquo;s, so many of Python&rsquo;s
syntactic constructs are not valid. However, the following constructs
<em>are valid</em>, despite being invalid in pure Lua:</p>

<ul>
<li><p>Iterating over table literals or table variables directly in a &ldquo;for&rdquo; loop:</p>

<pre><code>{% for i in {1, 2, 3} %}...{% endfor %}
</code></pre></li>
<li><p>Conditional loops via an &ldquo;if&rdquo; expression suffix:</p>

<pre><code>{% for x in range(10) if is_odd(x) %}...{% endfor %}
</code></pre></li>
<li><p>Table unpacking for list elements when iterating through a list of lists:</p>

<pre><code>{% for a, b, c in {{1, 2, 3}, {4, 5, 6}} %}...{% endfor %}
</code></pre></li>
<li><p>Default values for macro arguments:</p>

<pre><code>{% macro m(a, b, c='c', d='d') %}...{% endmacro %}
</code></pre></li>
</ul>
</li>
<li><p>Strings do not have unicode escapes nor is unicode interpreted in any way.</p></li>
</ul>


<h3 id="Syntactic.Differences">Syntactic Differences</h3>

<ul>
<li>Line statements are not supported due to parsing complexity.</li>
<li>In <code>{% for ... %}</code> loops, the <code>loop.length</code>, <code>loop.revindex</code>,
<code>loop.revindex0</code>, and <code>loop.last</code> variables only apply to sequences, where
Lua&rsquo;s <code>'#'</code> operator applies.</li>
<li>The <code>{% continue %}</code> and <code>{% break %}</code> loop controls are not supported due to
complexity.</li>
<li>Loops may be used recursively by default, so the <code>recursive</code> loop modifier is
not supported.</li>
<li>The <code>is</code> operator is not supported by Lua, so tests of the form <code>{{ x is y }}</code>
should be written <code>{{ is_y(x) }}</code> (e.g. <code>{{ is_number(42) }}</code>).</li>
<li>Filters cannot occur after tokens within an expression (e.g.
<code>{{ "foo"|upper .. "bar"|upper }}</code>), but can only occur at the end of an
expression (e.g. <code>{{ "foo".."bar"|upper }}</code>).</li>
<li>Blocks always have access to scoped variables, so the <code>scoped</code> block modifier
is not supported.</li>
<li>Named block end tags are not supported since the parser cannot easily keep
track of that state information.</li>
<li>Any <code>{% block ... %}</code> tags within a &ldquo;false&rdquo; block (e.g. <code>{% if a %}</code> where <code>a</code>
evaluates to <code>false</code>) are never read and stored due to the parser
implementation.</li>
<li>Inline &ldquo;if&rdquo; expressions (e.g. <code>{% extends b if a else c %}</code>) are not
supported. Instead, use a Lua conditional expression
(e.g. <code>{% extends a and b or c %}</code>).</li>
<li>Any <code>{% extends ... %}</code> tags within a sub-scope are not effective outside that
scope (e.g. <code>{% if a %}{% extends a %}{% else %}{% extends b %}{% endif %}</code>).
Instead, use a Lua conditional expression (e.g. <code>{% extends a or b %}</code>).</li>
<li>Macros are simply Lua functions and have no metadata attributes.</li>
<li>Macros do not have access to a <code>kwargs</code> variable since Lua does not support
keyword arguments.</li>
<li><code>{% from x import y %}</code> tags are not supported. Instead, you must use either
<code>{% import x %}</code>, which imports all globals in <code>x</code> into the current
environment, or use <code>{% import x as z %}</code>, which imports all globals in <code>x</code>
into the variable <code>z</code>.</li>
<li><code>{% set ... %}</code> does not support multiple assignment. Use <code>{% do ...%}</code>
instead. The catch is that <code>{% do ... %}</code> does not support filters.</li>
<li>The <code>{% trans %}</code> and <code>{% endtrans %}</code> tags, <code>{% with %}</code> and <code>{% endwith %}</code>
tags, and <code>{% autoescape %}</code> and <code>{% endautoescape %}</code> tags are not supported
since they are outside the scope of this implementation.</li>
</ul>


<h3 id="Filter.Differences">Filter Differences</h3>

<ul>
<li>Only the <code>batch</code>, <code>groupby</code>, and <code>slice</code> filters return generators which
produce one item at a time when looping. All other filters that produce
iterable results generate all items at once.</li>
<li>The <code>float</code> filter only works in Lua 5.3 since that version of Lua has a
distinction between floats and integers.</li>
<li>The <code>safe</code> filter must appear at the end of a filter chain since its output
cannot be passed to any other filter.</li>
</ul>


<h3 id="Function.Differences">Function Differences</h3>

<ul>
<li>The global <code>range(n)</code> function returns a sequence from 1 to <code>n</code>, inclusive,
since lists start at 1 in Lua.</li>
<li>No <code>lipsum()</code>, <code>dict()</code>, or <code>joiner()</code> functions for the sake of simplicity.</li>
</ul>


<h3 id="API.Differences">API Differences</h3>

<ul>
<li><p>Lupa has a much simpler API consisting of just four functions and three
fields:</p>

<ul>
<li><code>lupa.expand()</code>: Expands a string template subject to an environment.</li>
<li><code>lupa.expand_file()</code>: Expands a file template subject to an environment.</li>
<li><code>lupa.configure()</code> Configures delimiters and template options.</li>
<li><code>lupa.reset()</code>: Resets delimiters and options to their defaults.</li>
<li><code>lupa.env</code>: The default environment for templates.</li>
<li><code>lupa.filters</code>: The set of available filters (<code>escape</code>, <code>join</code>, etc.).</li>
<li><code>lupa.tests</code>: The set of available tests (<code>is_odd</code>, <code>is_defined</code>, etc.).</li>
</ul>
</li>
<li><p>There is no bytecode caching.</p></li>
<li>Lupa has no extension mechanism. Instead, modify <code>lupa.env</code>, <code>lupa.filters</code>,
and <code>lupa.tests</code> directly. However, the parser cannot be extended.</li>
<li>Sandboxing is not supported, although <code>lupa.env</code> is safe by default (<code>io</code>,
<code>os.execute</code>, <code>os.remove</code>, etc. are not available).</li>
</ul>


        </div>
        <div id="footer">
          <p style="text-align:center;"> &copy; 2015 Mitchell mitchell.att.foicica.com</p>

        </div>
      </div>
    </body>
  </html>
