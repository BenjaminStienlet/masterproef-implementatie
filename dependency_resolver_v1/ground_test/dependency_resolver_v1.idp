vocabulary V {
    // ==================
    // ==== Packages ====
    // ==================
    type PackageName
    type VersionNumber isa nat
    
    // A versioned package is a specific version of a package that can be installed
    type VersionedPackageType constructed from {versionedPackage(PackageName, VersionNumber)}
    type VersionedPackage isa VersionedPackageType
    
    // A virtual package is a package that is provided by a versioned package
    // A virtual package can't be installed.
    type VirtualPackageType constructed from {virtualPackage(PackageName, VersionNumber), virtualPackage(PackageName)}
    type VirtualPackage isa VirtualPackageType
    
    isInstalledBefore(VersionedPackage)
    isInstalledAfter(VersionedPackage)
    
    // Predicates to indicate the changes between isInstalledBefore/1 and isInstalledAfter/1
    isRemoved(VersionedPackage)
    isNewlyInstalled(VersionedPackage)
    isUpgraded(VersionedPackage, VersionedPackage)
    
    isNotUpToDate(VersionedPackage)
    
    
    // =============================
    // ==== Package constraints ====
    // =============================
    // Supported comparisons: =, !=, >, >=, <, <=
    type RelationOperator constructed from {EQ, NEQ, GT, GE, LT, LE}
    
    // A package version constraint can be used in dependency- and conflicts-relations, and in requests
    type PackageVersionConstraintType constructed from {packageVersionConstraint(PackageName), packageVersionConstraint(PackageName, RelationOperator, VersionNumber)}
    type PackageVersionConstraint isa PackageVersionConstraintType
    
    isFulfilledConstraint(PackageVersionConstraint)
    isFulfilledConstraintBy(PackageVersionConstraint, VersionedPackage)
    
    relationHolds(VersionNumber, RelationOperator, VersionNumber)
    
    
    // ======================
    // ==== Dependencies ====
    // ======================
    type OrDependencyType constructed from {orDependency(PackageVersionConstraint, OrDependencyType), EmptyOrDep}
    type OrDependency isa OrDependencyType
    
    type DependencyType constructed from {dependency(OrDependency), TrueDep, FalseDep}
    type Dependency isa DependencyType
    
    depends(VersionedPackage, Dependency)
    
    conflicts(VersionedPackage, PackageVersionConstraint)
    
    // Note: if the provided package does not include a version, then it provides every possible version
    provides(VersionedPackage, VirtualPackage)
    
    isFulfilledDependency(Dependency)
    // For an orDependency one of the package version constraints has to be fulfilled
    isFulfilledOrDependency(OrDependency)
    // Checks if a package version constraint is fulfilled by a virtual package
    isFulfilledProvides(PackageVersionConstraint)
    isFulfilledProvidesBy(PackageVersionConstraint, VersionedPackage)

    
    // =======================
    // ==== Keep requests ====
    // =======================
    // The user can request to keep the version / package / feature(s) (virtual package) of a versioned package
    type KeepType constructed from {Version, Package, Feature, None}
    
    keep(VersionedPackage, KeepType)
    
    
    // ==================
    // ==== Requests ====
    // ==================
    type RequestType constructed from {Install, Remove, Upgrade}
    
    request(RequestType, PackageVersionConstraint)
}

vocabulary OutputVoc {
    extern V::isInstalledBefore/1
    extern V::isInstalledAfter/1
    extern V::isRemoved/1
    extern V::isNewlyInstalled/1
    extern V::isUpgraded/2
    extern V::isNotUpToDate/1
}

theory T:V {
    // ======================
    // ==== Dependencies ====
    // ======================
    // All dependencies have to be fulfilled for the installed packages
    ! vp[VersionedPackage] dep[Dependency]: depends(vp, dep) & isInstalledAfter(vp) 
    	=> isFulfilledDependency(dep).
    // For every conflict the package is not installed; or the constraint is not fulfilled; 
    //   or the constraint is only fulfilled by this package (allow self-conflicts)
    ! vp[VersionedPackage] pvc[PackageVersionConstraint]: conflicts(vp, pvc) & isInstalledAfter(vp) 
   		=> ~isFulfilledConstraint(pvc) 
    		| (isFulfilledConstraintBy(pvc, vp) & ~? vp2: vp ~= vp2 & isFulfilledConstraintBy(pvc, vp2)).
    
    
    // =====================
    // ==== isFulfilled ====
    // =====================
    { // isFulfilled(Dependency)
        isFulfilledDependency(TrueDep()).
        isFulfilledDependency(FalseDep()) <- false.
        // dependency(OrDependency) is fulfilled iff OrDependency is fulfilled
        ! orDep[OrDependency]: isFulfilledDependency(dependency(orDep)) <- isFulfilledOrDependency(orDep).
    }
        
    { // isFulfilled(OrDependency)
        isFulfilledOrDependency(EmptyOrDep()) <- false.
        // orDependency(PVC, OrDependency) is fulfilled iff PVC is fulfilled or OrDependency is fulfilled
        ! pvc[PackageVersionConstraint] orDep[OrDependency]: isFulfilledOrDependency(orDependency(pvc, orDep)) 
        	<- isFulfilledConstraint(pvc) | isFulfilledOrDependency(orDep).
    }
    
    { // isFulfilled(PackageVersionConstraint)
    	! pvc[PackageVersionConstraint]: isFulfilledConstraint(pvc) 
        	<- ? vp[VersionedPackage]: isFulfilledConstraintBy(pvc, vp).
        // pvc(PackageName) is fulfilled when there exists a package version with PackageName that is installed;
        //   or there exists a package, that provides the given package, that is installed
        ! pkg[PackageName] vp[VersionedPackage]: isFulfilledConstraintBy(packageVersionConstraint(pkg), vp) 
        	<- (isInstalledAfter(vp) & (?1 v[VersionNumber]: vp = versionedPackage(pkg, v)))
        		| isFulfilledProvidesBy(packageVersionConstraint(pkg), vp).
        // pvc(PackageName, RelOp, VersionNumber) is fulfilled when there exists a package version, with
        //   PackageName and a valid version number, that is installed; or when there exists a package, 
        //   that provides the required package, that is installed
        ! pkg[PackageName] rel[RelationOperator] v[VersionNumber] vp[VersionedPackage]:
        	isFulfilledConstraintBy(packageVersionConstraint(pkg, rel, v), vp) 
        	<- (isInstalledAfter(vp) & 
        		(?1 v2[VersionNumber]: vp = versionedPackage(pkg, v2) & relationHolds(v2, rel, v)))
        		| isFulfilledProvidesBy(packageVersionConstraint(pkg, rel, v), vp).
    }
    
    { // isFulfilledProvides(PackageVersionConstraint) -- check if a pvc is fulfilled by a provides relation
    	! pvc[PackageVersionConstraint]: isFulfilledProvides(pvc) 
        	<- ? vp[VersionedPackage]: isFulfilledProvidesBy(pvc, vp).
        // A pvc(PackageName) is fulfilled by a provides relation if there exists a versioned package,
        //   that provides a package with PackageName, that is installed
        ! pkg[PackageName] vp[VersionedPackage]: isFulfilledProvidesBy(packageVersionConstraint(pkg), vp) 
        	<- isInstalledAfter(vp) & 
        		(provides(vp, virtualPackage(pkg)) | ? v[VersionNumber]: provides(vp, virtualPackage(pkg, v))).
        // A pvc(PackageName, RelOp, VersionNumber) is fulfilled by a provides relation if there exsists a
        //   versioned package, that provides a package with PackageName and a valid version number 
        //   (if a VN is specified), that is installed
        ! pkg[PackageName] rel[RelationOperator] v[VersionNumber] vp[VersionedPackage]:
        	isFulfilledProvidesBy(packageVersionConstraint(pkg, rel, v), vp)
        	<- isInstalledAfter(vp) & (provides(vp, virtualPackage(pkg))
        		| (? v2[VersionNumber]: relationHolds(v2, rel, v) & provides(vp, virtualPackage(pkg, v2)))).
    }
    
    // =======================
    // ==== Keep requests ====
    // =======================
    // Preserve the current version
    ! vp[VersionedPackage]: keep(vp, Version()) & isInstalledBefore(vp) => isInstalledAfter(vp).
    // Preserve at least one version of the package
    ! pkg[PackageName] v[VersionNumber]: keep(versionedPackage(pkg, v), Package()) 
    	& isInstalledBefore(versionedPackage(pkg, v)) 
    	=> ? v2[VersionNumber]: isInstalledAfter(versionedPackage(pkg, v2)).
    // Preserve all the provided features
    ! vp[VersionedPackage] virt[VirtualPackage]: keep(vp, Feature()) & provides(vp, virt) & isInstalledBefore(vp) 
    	=> ? vp2[VersionedPackage]: isInstalledAfter(vp2) & (provides(vp2, virt) | 
    	(! pkg[PackageName] v[VersionNumber]: virt = virtualPackage(pkg, v) => provides(vp2, virtualPackage(pkg)))).
	
    
    // =======================
    // ==== relationHolds ====
    // =======================
    { // relationHolds(VersionNumber, RelationOperator, VersionNumber)
        ! v1[VersionNumber] v2[VersionNumber]: relationHolds(v1, EQ(), v2)  <- v1 = v2.
        ! v1[VersionNumber] v2[VersionNumber]: relationHolds(v1, NEQ(), v2) <- v1 ~= v2.
        ! v1[VersionNumber] v2[VersionNumber]: relationHolds(v1, GT(), v2)  <- v1 > v2.
        ! v1[VersionNumber] v2[VersionNumber]: relationHolds(v1, GE(), v2)  <- v1 >= v2.
        ! v1[VersionNumber] v2[VersionNumber]: relationHolds(v1, LT(), v2)  <- v1 < v2.
        ! v1[VersionNumber] v2[VersionNumber]: relationHolds(v1, LE(), v2)  <- v1 =< v2.
    }
    
    
    // ==================
    // ==== Requests ====
    // ==================
    // For each install request, the package version constraint has to be fulfilled
    ! pvc[PackageVersionConstraint]: request(Install(), pvc) => isFulfilledConstraint(pvc).
    // For each remove request, the package version constraint is not fulfilled
    ! pvc[PackageVersionConstraint]: request(Remove(), pvc) => ~isFulfilledConstraint(pvc).
    // Upgrade: exactly one version remains after the upgrade of a package and this version is greater or equal than
    //   the greatest version of the package    
    ! pvc[PackageVersionConstraint]: request(Upgrade(), pvc) 
    	=> ?1 vp[VersionedPackage]: isFulfilledConstraintBy(pvc, vp) 
    	& ! package[PackageName] v1[VersionNumber]: vp = versionedPackage(package, v1)
    	=> (! v2[VersionNumber]: isInstalledAfter(versionedPackage(package, v2)) => v1 = v2)
    	& (! v2[VersionNumber]: isInstalledBefore(versionedPackage(package, v2)) => v1 >= v2).
    
    
    // =========================
    // ==== Package changes ====
    // =========================
    {
        // A versioned package is removed if it is in isInstalledBefore/1, but not in isInstalledAfter/1 
        //     and the versioned package is not upgraded
      	! vp[VersionedPackage]: isRemoved(vp) 
        	<- isInstalledBefore(vp) & ~isInstalledAfter(vp) & ~(? vp2[VersionedPackage]: isUpgraded(vp2, vp)).
        // A versioned package is newly installed if it is not in isInstalledBefore/1, but it is in isInstalledAfter/1 
        //     and the versioned package is not upgraded
        ! vp[VersionedPackage]: isNewlyInstalled(vp) 
        	<- ~isInstalledBefore(vp) & isInstalledAfter(vp) & ~(? vp2[VersionedPackage]: isUpgraded(vp2, vp)).
        // A versioned package is upgraded to another versioned package if the first VP is in isInstalledBefore/1, 
        //     the second VP is in isInstalledAfter/1, and the version number of the second VP is strictly larger
    	! package[PackageName] v1[VersionNumber] v2[VersionNumber]: 
        	isUpgraded(versionedPackage(package, v2), versionedPackage(package, v1))
        	<- isInstalledAfter(versionedPackage(package, v1))
    		& isInstalledBefore(versionedPackage(package, v2)) & v1 > v2.
    }
    
    
    // ===============
    // ==== Other ====
    // ===============
    {
        // A versioned package is not up to date if there exists another versioned package with the same package name
        //     and a strictly larger version number
        ! v1[VersionNumber] package[PackageName]: isNotUpToDate(versionedPackage(package, v1)) <- 
        	isInstalledAfter(versionedPackage(package, v1)) &
        	? vp2[VersionedPackage] v2[VersionNumber]: vp2 = versionedPackage(package, v2) & v2 > v1.
    }
}

// Number of packages that have been removed, newly installed or upgraded
term min_removed_added_upgraded:V {
    #{vp[VersionedPackage]: isRemoved(vp) | isNewlyInstalled(vp) | ? vp2[VersionedPackage]: isUpgraded(vp2, vp)}
}

// Number of packages that are not up to date
term min_not_upgraded:V {
    #{vp[VersionedPackage]: isNotUpToDate(vp)}
}

// Paranoid: minimizing the number of packages removed in the solution, and also the packages changed by the solution (lexicographic)
term paranoid:V {
    10000 * #{vp[VersionedPackage]: isRemoved(vp)} + #{vp[VersionedPackage]: ? vp2[VersionedPackage]: isUpgraded(vp, vp2)}
}